# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:
#	settare correttamente distmapt
#	verificare parametri ltr_finder parameters -D -d -L -l mediante statistica da repBase
#	set_collapse do not respect the order of input
#	test behaviour with nested ltrs
#	capire come si comporta LTR finder con elementi nested
# 	confrontare con dati gff
# 	usare dati generati da mirko

REFERENCE ?=
CHUNCK_SIZE ?= 5000000
MAX_LTRS_DIST ?= 20000
MIN_LTRS_DIST ?= 1000
MAX_LTR_LEN ?= 3500
MIN_LTR_LEN ?= 100
SUB_RATE ?= 1.3e-08
TRNA ?=





split_fasta.mk: reference.fasta
	splitter \
	-sformat fasta \
	-filter \
	-size=$(CHUNCK_SIZE) \
	-overlap=$$(echo "500 + $(MAX_LTRS_DIST) + 2 * $(MAX_LTR_LEN)" | bc) \   * overlap is calculated on  the basis of maax distance between ltrs and max length of ltrs plus some bp *
	-ossingle $<; \
	printf "CHUNCKS := \$$(wildcard *_*-*.fasta)">$@

# needed for initialize variablee $(CHUNCKS)
include split_fasta.mk


LTR_FINDERS = $(CHUNCKS:.fasta=.ltr_finder)
%.ltr_finder: %.fasta tRNA.fasta ps_scan split_fasta.mk
	ltr_finder \
	$(ltr_finder_param) \
	-s $^2 \   * tRNA sequence file *
	$^1 >$@

# define ltr_finder_param
# 	-D $(MAX_LTRS_DIST) \   * Max distance between LTRS, default is 20000 *
# 	-d $(MIN_LTRS_DIST) \   * Min distance between LTRs, default is 1000 *
# 	-L $(MAX_LTR_LEN) \   * Max length of 5-3LTR, default is 3500 *
# 	-l $(MIN_LTR_LEN) \   * Min length of 5-3LTR, default is 100 *
# 	-E \   * LTR must have edge signal (at least two of PBS,PPT,TSR) *
# 	-F 10000000000 \   * 5-LTR must have TG *
# 	-F 00010000000 \   * 3-LTR must have CA *
# 	-F 00001000000 \   * TSR must be found *
# 	-F 00000100000 \   * PBS must be found *
# 	-F 00000010000 \   * PPT must be found *
# 	-F 00000001000 \   * RT domain muste be found *
# 	-w 2 \   * output format: table. *
#	-a $^3 \   * Use ps_scan to predict protein domain *
# endef


TABS = $(LTR_FINDERS:.ltr_finder=.tab)
%.tab: %.ltr_finder
	gawk -F'\t' 'BEGIN{OFS="\t";} /^\[.[0-9]+\]\t/ { \
	  out=""; \
	  gsub(/[\[+,\]+,[:blank:]]/,"",$$1); \
	  # print 1 2 \
	  printf "%s\t%s",$$1,$$2; \
	  split($$3,b,"-"); \
	  # print 3 \
	  for(i=1;i<=length(b);i++) {printf "\t%s",b[i]}; \
	  split($$4,c,","); \
	  # print 4 \
	  for(i=1;i<=length(c);i++) {printf "\t%s",c[i]}; \
	  # print rest \
	  for(i=5;i<=NF;i++) {printf "\t%s",$$i}; \
	  printf "\n"; } ' <$< >$@


LTRS1 = $(TABS:.tab=.ltr1)
%.ltr1: %.tab %.fasta
	>$@; \
	bawk '!/^[$$,\#+]/ { \
	  start_1=$$3; \
	  end_1=$$3+$$5; \
	  cmd_1="seqret -sformat fasta -filter -sbegin "start_1" -send "end_1" <$^2 \
	  | descseq -sformat fasta -filter -name \"_"start_1"-"end_1"\" -append >>$@"; \
	  system(cmd_1); \
	  }' <$<


LTRS2 = $(TABS:.tab=.ltr2)
%.ltr2: %.tab %.fasta
	>$@; \
	bawk '!/^[$$,\#+]/ { \
	  start_2=$$4-$$6; \
	  end_2=$$4; \
	  cmd_2="seqret -sformat fasta -filter -sbegin "start_2" -send "end_2" <$^2 \
	  | descseq -sformat fasta -filter -name \"_"start_2"-"end_2"\" -append >>$@"; \
	  system(cmd_2); \
	  }' <$<


ALN = $(TABS:.tab=.aln)
%.aln: %.ltr1 %.ltr2
	for i in $$(seq 0 $$(echo $$(fasta_count $<)-1 | bc)); do \
	  if [ -s $< ] && [ -s $^2 ]; then \
	    mkdir -p $@; \
	    stretcher -sformat fasta \
	    -filter \
	    -asequence <(fasta_get @$$i <$<) \
	    -bsequence <(fasta_get @$$i <$^2) \
	    -aformat msf \
	    >$@/$$i.$*.aln; \
	  fi \
	done



# The output from the program is a file containing a matrix of the 
# calculated distances between each of the input aligned sequences.
# The distances are expressed in terms of the number of substitutions 
# per 100 bases or amino acids.
MTX = $(ALN:.aln=.mtx)
%.mtx: %.aln
	for i in $</*.aln; do \
	  if [ -s $$i ]; then \
	    mkdir -p $@; \
	    distmat -sformat1 msf \
	    -filter \
	    -nucmethod 2 \
	    <$$i >$@/$$(basename $${i%.*}).mtx; \
	  fi \
	done


# MYA (Kdx*1e-02*1e-06)/(2*mu)
DST = $(MTX:.mtx=.dst)
%.dst: %.mtx
	printf "# chr\tchunk_start\tchunck_end\ttrp_start\ttrp_end\tage\n" >$@; \
	for i in $</*.mtx; do \
	  if [ -s $$i ]; then \
	    tr -s "\n" "\t" <$$i \
	    | bawk '!/^[$$,\#+]/ { \
	        # remove spaces \
	        gsub(/[ \t]+$$/, "", $$9); \
	        # calculate time of divergence \
	        MYA=($$9*1e-08)/($(SUB_RATE)*2); \
	        # remove sequence numbers \
	        split($$10,a,/[_-]/); \
	        split($$12,b,/[_-]/); \
	        TRP_START=a[2]+a[4]-1; \
	        TRP_END=b[2]+b[5]-1; \
	        printf "%s\t%i\t%i\t%i\t%i\t%.4f\n",a[1],b[2],a[3],TRP_START,TRP_END,MYA; \
	      }' >>$@; \
	  fi \
	done

# put all together
dst.sort: $(DST)
	cat $^ \
	| bawk '!/^[$$,\#+]/ { print $$0 }' \
	| set_collapse -o 6 \
	| bsort --key=1,1n --key=2,2n --key=4,4n --key=5,5n >$@




.PHONY: test
test:
	@echo DST = $(CHUNCKS:.mtx=.dst)



# prerequisites of target all.
# automatically deleted when clean.full
ALL +=  reference.fasta \
	tRNA.fasta \
	split_fasta.mk \
	$(CHUNCKS) \
	$(LTR_FINDERS) \
	$(TABS) \
	$(LTRS1) \
	$(LTRS2) \
	$(ALN) \
	$(MTX) \
	$(DST) \
	dst.sort

# clean from intermediate and temporary files
CLEAN += $(CHUNCKS) \
	 $(LTR_FINDERS) \
	 $(TABS) \
	 $(LTRS1) \
	 $(LTRS2) \
	 $(ALN) \
	 $(MTX) \
	 $(DST)

# deletes as soon as no longer needed
# added to the CLEAN list
INTERMEDIATE +=
