# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:
#	coordinate destra assoluta
#	usare il file di mappaggio dei reference usato nella fase 1
#	usare il file di mappaggio degli elementi prodotto nella phase_1
#	produrre da questo 2 file: uno per la porzione interna e uno per le estremità

TE_OTHERS	?=
TE_REPBASE	?=
DELITIONS	?=
REF_CUT_LEN     ?= 200
TE_CUT_LEN	?= 100

extern ../phase_1/reference.fasta.cleaned.gz as LOCAL_REFERENCE

# link to reference
reference.gz:
	ln -sf $(LOCAL_REFERENCE) $@
# link to te sequences from repbase
te.repbase.gz:
	ln -sf $(TE_REPBASE) $@
# link to te sequences from Dario
te.others.gz:
	ln -sf $(TE_OTHERS) $@



# map between name of transposable elements in RepBase end name defined by Wicker et al.
te.repbase.map: te.repbase.gz
	zcat $< \
	| sed -e 's/\t/ /g' -e 's/\\/ /g' -e 's/,/ /g' -e 's/\.\./ /g' \   * remove annoying characters *
	| fasta2tab \
	| bawk 'function randint(n) { return int(n * rand()); } \
	BEGIN {i=1; IGNORECASE=1;} !/^[$$,\#+]/ \
	{  i++; \
	if ( $$1 ~ /Gypsy/ || $$1 ~ /GYP/ ) { lab="RLG"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Cop/ ) { lab="RLC"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Bel-Pao/ ) { lab="RLB"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Retrovirus/ ) { lab="RLR"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /ERV/ ) { lab="RLE"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /DIRS/ ) { lab="RYD"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Ngaro/ ) { lab="RYN"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /VIPER/ ) { lab="RYV"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /RTE/ ) { lab="RIT"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Jocket/ ) { lab="RIJ"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ / L1 / || $$1 ~ /LINE/ || $$1 ~ /RIL/ ) { lab="RIL"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /SINE/ ) { lab="RSX"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /tRNA/ ) { lab="RSX"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /7SL/ ) { lab="RSX"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /5S/ ) { lab="RSX"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Mariner/ ) { lab="DTT"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /hAT / ) { lab="DTA"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Mutator/ || $$1 ~ /MuDR/ ) { lab="DTM"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Merlin/ ) { lab="DTE"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Transib/ ) { lab="DTR"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /PiggyBac/ ) { lab="DTB"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Harb/ || $$1 ~ /PIF/ ) { lab="DTH"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /CACTA/ || $$1 ~ /EnSpm/ ) { lab="DTC"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Crypton/ ) { lab="DYC"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Helitron/ ) { lab="DHH"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Maverick/ ) { lab="DMM"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Penelope/ ) { lab="RPP"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else {print $$1 >"te.repbase.unamed";} }' \   * seve to file sequences that can't be classified *
	| bawk '!/^[$$,\#+]/ \
	{ if ( $$1 !~ /[-,\_][I]/ ) { print $$0; } \
	else { print >"$(basename $@).inner"; } }' >$@   * remove sequences from inner of element *

# contains sequences from repbase tha can't be 
# classified with the name defined by Wicker et al
te.repbase.unamed: te.repbase.map
	touch $@


# contains only renamed trasposase
te.repbase.inner: te.repbase.map
	touch $@


# rename headers of te from RepBase according to new name defined by Wicker et al.
te.repbase.fasta: te.repbase.map te.repbase.gz
	zcat $^2 \
	| sed -e 's/\t/ /g' -e 's/\\/ /g' -e 's/,/ /g' -e 's/\.\./ /g' \   * remove annoying characters *
	| fasta2tab \
	| translate -k $< 1 \
	| bsort \
	| tab2fasta 2 \
	| seqret -filter -supper >$@



# unzip and remove annoying charachters
te.others.fasta: te.others.gz
	zcat $< \
	| sed -e 's/\t/ /g' -e 's/\\/ /g' -e 's/,/ /g' -e 's/\.\./ /g' \   * remove annoying characters *
	| seqret -filter -supper >$@



# mapping of the IDs of the sequences to numbers
name.maps: reference.gz
	zcat $< \
	| fasta2tab \
	| bawk 'BEGIN {i=1;} !/^[$$,\#+]/ { print i++,$$1 }' \
	| sort -k 2,2n >$@


# change id of sequences to numbers
reference.norm.fasta: reference.gz
	zcat $< \
	| seqret -filter -supper \
	| fasta2tab \
	| bawk 'BEGIN {i=1;} !/^[$$,\#+]/ { print i++,$$2 }' \
	| bsort -k 1,1n \
	| tab2fasta -s 2 >$@


# extract left-side of reference 
reference.left.fasta: reference.norm.fasta
	seqret -filter -sbegin 0 -send $(REF_CUT_LEN) <$< >$@


# extract right-side of reference 
reference.right.fasta: reference.norm.fasta
	seqret -filter -sbegin -$(REF_CUT_LEN) <$< >$@


# extract left and right side of te lists and put them together 
te.left-right.fasta: te.repbase.fasta te.others.fasta
	cat $< $^2 \
	| seqret -filter -sbegin 0 -send $(TE_CUT_LEN) \   * extract first 100 bp *
	| fasta2tab \
	| bawk '!/^[$$,\#+]/ { print $$1"_left",$$2 }' \
	| bsort \
	| tab2fasta 2 >$@ \
	&& cat $< $^2 \
	| seqret -filter -sbegin -$(TE_CUT_LEN) \   * extract last 100 bp *
	| fasta2tab \
	| bawk '!/^[$$,\#+]/ { print $$1"_right",$$2 }' \
	| bsort \
	| tab2fasta 2 >>$@


# run RepeatMasker
.PRECIOUS: reference.%.fasta.out
reference.%.fasta.out: te.left-right.fasta reference.%.fasta
	!threads
	RepeatMasker \
	-s \
	-no_is \
	-nolow \
	-pa $$THREADNUM \
	-lib $< \
	$^2


# reformat RepeatMasker to Bed format
reference.%.out.bed: reference.%.fasta.out reference.norm.fasta
	tr -s ' ' \\t <$< \
	| sed 's/^[ \t]*//;s/[ \t]*$$//' \
	| bawk -v reference_fasta=$^2 'function abs(x){return ((x < 0.0) ? -x : x)} \
	BEGIN {strand=""; start=""; end="";} /^[0-9]+/ \
	{ if ( $$9 == "C" ) { strand="-"; } \
	else if ( $$9 == "+" ) { strand="+"; } \
	print $$5,$$6,$$7,$$10,$$1,strand,$$12,$$13,"255";}' >$@


# collapse alignments that ara 80 bp apart
# filter-out alignments less than 80% REF_CUT_LEN
reference.%.out.bed.collapsed: reference.%.out.bed
	SHORT_ALN=$(basename $@).short; \
	>$$SHORT_ALN; \   * reset file *
	bedtools merge -d 80 -nms -scores collapse -i $< \   * collapse features that ara 80 bp apart *
	| bawk -v short_aln=$$SHORT_ALN  'function abs(x){return ((x < 0.0) ? -x : x)} \
	/^[0-9]+/ \
	{ if ( abs($$3-$$2) >= int($(TE_CUT_LEN)/100*80) ) \   * filter-out alignments less than 80% REF_CUT_LEN *
	{ print $$0; } \
	else { print $$0 >>short_aln; } }' \
	| bsort -n --key=1,1 >$@

reference.%.out.bed.short: reference.%.out.bed.collapsed
	touch $@


# assign deletions to the te class, on the basis of the homogeneity on match 
# found with transposable elements
reference.%.out.bed.merged: reference.%.out.bed.collapsed
	NOT_MERGED=$(basename $@).notmerged; \
	>$$NOT_MERGED; \   * reset file *
	bawk -v not_merged=$$NOT_MERGED 'function max(x){i=0;for(val in x){if(i<=x[val]){i=x[val];}} return i;} \   * function to get the maximum of a vector *
	/^[0-9]+/ \
	{ split( $$4, teFullName, ";"); \   * split te names *
	consistent="TRUE"; \
	teClass=substr(teFullName[1], 1, 3); \   * extract the firs 3 letters (the class) *
	for (i=2; i<=length(teFullName); i++) { \
	if ( substr(teFullName[i], 1, 3) != teClass ) { consistent="FALSE"; } \   * check consistentency *
	} \
	split( $$5, score, ","); \   * split scores *
	if ( consistent=="TRUE" ) { print $$1,$$2,$$3,teClass,max(score),"$*"; } \
	else { print $$0 >>not_merged; } \
	}' $< >$@

reference.%.out.bed.notmerged: reference.%.out.bed.merged
	touch $@

# classifie reference sequences on the basis of identically classified ends
reference.classified: reference.left.out.bed.merged reference.right.out.bed.merged
	UNCLASSIFIED=$(basename $@).inconsistent; \
	>$$UNCLASSIFIED; \   * reset file *
	cat $^ \
	| bsort -n --key=1,1 \
	| sed 's/\t/TAB/' \
	| tr '\t' ' ' \
	| sed 's/TAB/\t/' \
	| set_collapse -o -g $$'\t' 2 \
	| bsort -n --key=1,1 \
	| tr ' ' \\t \
	| bawk -v unclassified=$$UNCLASSIFIED  '/^[0-9]+/ \
	{ if ( $$4 == $$9 ) \
	{ print $$1,$$2,$$3,$$5,$$4,$$6,$$7,$$8,$$10,$$9,$$11; } \
	else { print $$1,$$2,$$3,$$5,$$4,$$6,$$7,$$8,$$10,$$9,$$11 >>unclassified; } }' >$@


# revert chr names to original ones
reference.classified.unmap: name.maps reference.classified
	translate $< 1 <$^2 \
	| sed '1i# chr\t\start\tend\tscore\tclass\tside\tstart\tend\tscore\tclass\tside' >$@



# classifie repeat with respect to class
RLG.classified.unmap: reference.classified.unmap
	bawk '!/^[$$,\#+]/ \
	{ print $$0 >$$5".classified.unmap"; }' $<

# if( system( "[ -f " $$5".classified.unmap ] " ) == 0 ) \   * reset file that already exists *
# 	{ close($$5".classified.unmap"); printf("") >$$5".classified.unmap"; } \   * to reset file in awk first close it *


# unclassified sequences
reference.unclassified.lst: reference.gz reference.classified.unmap
	comm -23 --check-order  \
	<(zcat $< | fasta2tab | cut -f1 | sort) \
	<(bawk '!/^[$$,\#+]/' $^2 | cut -f1 | sort) >$@

# unclassified sequences to fasta
reference.unclassified.fasta: reference.unclassified.lst reference.gz
	zcat $^2 | fasta_get -f $< >$@


# unclassified reference sequences
reference.inconsistent: reference.classified
	touch $@

# revert chr names to original ones
reference.inconsistent.unmap: name.maps reference.inconsistent
	translate $< 1 <$^2 \
	| sed '1i# chr\t\start\tend\tscore\tclass\tside\tstart\tend\tscore\tclass\tside' >$@
	
############################################################################################

# extract deletions that present match only at one of the two ends
reference.solo: reference.inconsistent
	select_columns 1 2 3 5 4 6 <$< \   * reswap columns due to swap in reference.classified *
	| bawk '/^[0-9]+/ { if ( $$7 !~ /^[0-9]+/ ) { print $$1,$$2,$$3,$$4,$$5,$$6; } \
	else { print $$0 >"$(basename $@).incoherent"; } }' >$@


# contains deletions that present match at both extremities with different Te
reference.incoherent: reference.solo
	touch $@


# extract sequences containing deletions that present match only at one of the two ends
reference.inner.fasta: reference.solo reference.norm.fasta
	fasta_get -f <(cut -f1 $<) <$^2 >$@


# extract fasta sequences of only renamed trasposase
te.repbase.inner.fasta: te.repbase.inner te.repbase.gz
	if [ -s $< ]; then \
	zcat $^2 \
	| sed -e 's/\t/ /g' -e 's/\\/ /g' -e 's/,/ /g' -e 's/\.\./ /g' \   * remove annoying characters *
	| fasta2tab \
	| translate -k $< 1 \
	| bsort \
	| tab2fasta 2 \
	| seqret -filter -supper >$@; \
	fi


# pattern rule can be used
# run RepeatMasker using trasposase sequences as database and solo sequences as queries
.PRECIOUS: reference.inner.fasta.out
reference.inner.fasta.out: te.repbase.inner.fasta reference.inner.fasta
	!threads
	RepeatMasker \
	-s \
	-no_is \
	-nolow \
	-pa $$THREADNUM \
	-lib $< \
	$^2


# pattern rule can be used
reference.inner.out.bed: reference.inner.fasta.out reference.norm.fasta
	tr -s ' ' \\t <$< \
	| sed 's/^[ \t]*//;s/[ \t]*$$//' \
	| bawk -v reference_fasta=$^2 'function abs(x){return ((x < 0.0) ? -x : x)} \
	BEGIN {strand=""; start=""; end="";} /^[0-9]+/ \
	{ if ( $$9 == "C" ) { strand="-"; } \
	else if ( $$9 == "+" ) { strand="+"; } \
	print $$5,$$6,$$7,$$10,$$1,strand,$$12,$$13,"255";}' >$@


# TEST A CORRECT METHOD TO MARGE ALIGNMENTS
reference.inner.out.bed.collapsed: reference.inner.out.bed
	SHORT_ALN=$(basename $@).short; \
	>$$SHORT_ALN; \   * reset file *
	bedtools merge -d 6000 -nms -scores collapse -i $< \   * collapse features that ara 80 bp apart *
	| bawk -v short_aln=$$SHORT_ALN  'function abs(x){return ((x < 0.0) ? -x : x)} \
	/^[0-9]+/ \
	{ if ( abs($$3-$$2) >= int($(TE_CUT_LEN)/100*80) ) \   * filter-out alignments less than 80% REF_CUT_LEN *
	{ print $$0; } \
	else { print $$0 >>short_aln; } }' \
	| bsort -n --key=1,1 >$@

# pattern rule can be used
reference.inner.out.bed.short: reference.inner.out.bed.collapsed
	touch $@

# # pattern rule can be used
# reference.inner.out.bed.merged: reference.inner.out.bed.collapsed
# 	NOT_MERGED=$(basename $@).notmerged; \
# 	>$$NOT_MERGED; \   * reset file *
# 	bawk -v not_merged=$$NOT_MERGED 'function max(x){i=0;for(val in x){if(i<=x[val]){i=x[val];}} return i;} \   * function to get the maximum of a vector *
# 	/^[0-9]+/ \
# 	{ split( $$4, teFullName, ";"); \   * split te names *
# 	consistent="TRUE"; \
# 	teClass=substr(teFullName[1], 1, 3); \   * extract the firs 3 letters (the class) *
# 	for (i=2; i<=length(teFullName); i++) { \
# 	if ( substr(teFullName[i], 1, 3) != teClass ) { consistent="FALSE"; } \   * check consistentency *
# 	} \
# 	split( $$5, score, ","); \   * split scores *
# 	if ( consistent=="TRUE" ) { print $$1,$$2,$$3,teClass,max(score),"$*"; } \
# 	else { print $$0 >>not_merged; } \
# 	}' $< >$@

# pattern rule can be used
reference.inner.out.bed.notmerged: reference.inner.out.bed.merged
	touch $@

# pattern rule can be used
reference.solo.classified: reference.solo reference.inner.out.bed.merged
	UNCLASSIFIED=$(basename $@).inconsistent; \
	>$$UNCLASSIFIED; \   * reset file *
	cat $^ \
	| bsort -n --key=1,1 \
	| sed 's/\t/TAB/' \
	| tr '\t' ' ' \
	| sed 's/TAB/\t/' \
	| set_collapse -o -g $$'\t' 2 \
	| bsort -n --key=1,1 \
	| tr ' ' \\t \
	| bawk -v unclassified=$$UNCLASSIFIED  '/^[0-9]+/ \
	{ if ( $$4 == $$9 ) \
	{ print $$1,$$2,$$3,$$5,$$4,$$6,$$7,$$8,$$10,$$9,$$11; } \
	else { print $$1,$$2,$$3,$$5,$$4,$$6,$$7,$$8,$$10,$$9,$$11 >>unclassified; } }' >$@

# pattern rule can be used
reference.solo.inconsistent: reference.solo.classified
	touch $@

# pattern rule can be used
reference.solo.classified.unmap: name.maps reference.solo.classified
	translate $< 1 <$^2 \
	| sed '1i# chr\t\start\tend\tscore\tclass\tside\tstart\tend\tscore\tclass\tside' >$@


# classifie repeat with respect to class
RLG.solo.classified.unmap: reference.solo.classified.unmap
	bawk '!/^[$$,\#+]/ \
	{ print $$0 >$$5".solo.classified.unmap"; }' $<


# unclassified sequences
reference.solo.unclassified.lst: reference.gz reference.solo.classified.unmap reference.classified.unmap
	comm -23 --check-order  \
	<(zcat $< | fasta2tab | cut -f1 | sort) \
	<(cat $^2 $^3 | bawk '!/^[$$,\#+]/' | cut -f1 | sort) >$@

# unclassified sequences to fasta
reference.solo.unclassified.fasta: reference.solo.unclassified.lst reference.gz
	zcat $^2 | fasta_get -f $< >$@

# 
# revert chr names to original ones
reference.solo.inconsistent.unmap: name.maps reference.solo.inconsistent
	translate $< 1 <$^2 \
	| sed '1i# chr\t\start\tend\tscore\tclass\tside\tstart\tend\tscore\tclass\tside' >$@


.PHONY: test
test:
	@echo

# prerequisites of target all.
# automatically deleted when clean.full
ALL +=  reference.gz \
	te.repbase.gz \
	te.others.gz \
	te.repbase.map \
	te.repbase.unamed \
	name.maps \
	reference.left.fasta \
	reference.right.fasta \
	te.left-right.fasta \
	reference.right.fasta.out \
	reference.left.fasta.out \
	reference.left.out.bed.collapsed \
	reference.right.out.bed.collapsed \
	reference.left.out.bed.merged \
	reference.right.out.bed.merged \
	reference.classified.unmap \
	reference.inconsistent.unmap \
	RLG.classified.unmap \
	reference.unclassified.lst \
	reference.unclassified.fasta \
	reference.inconsistent \
	reference.solo \
	reference.incoherent \
	reference.inner.fasta \
	te.repbase.inner.fasta \
	reference.inner.fasta.out \
	reference.inner.out.bed \
	reference.inner.out.bed.collapsed \
	reference.inner.out.bed.short \
	reference.inner.out.bed.merged \
	reference.inner.out.bed.notmerged \
	reference.solo.classified \
	reference.solo.inconsistent \
	reference.solo.classified.unmap \
	RLG.solo.classified.unmap \
	reference.solo.unclassified.lst \
	reference.solo.unclassified.fasta \
	reference.solo.inconsistent.unmap


# clean from intermediate and temporary files
CLEAN += reference.left.out.bed.short \
	 reference.right.out.bed.short \
	 reference.left.out.bed.notmerged \
	 reference.right.out.bed.notmerged \
	 $(wildcard *.unmap) \
	 $(wildcard reference.*.fasta.*)


# deletes as soon as no longer needed
# added to the CLEAN list
INTERMEDIATE += te.repbase.fasta \
		te.others.fasta \
		reference.norm.fasta \
		reference.right.out.bed \
		reference.left.out.bed \
		reference.classified \
		reference.inconsistent \
		te.repbase.inner