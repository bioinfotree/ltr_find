# Copyright 2013 Gabirele Magris <gmagris@appliedgenomics.org>

# TODO:
#	usare il file di mappaggio dei reference prodotto nella fase 1
#	al posto di te.repbase.map usare il file di mappaggio delle estremità prodotto nella fase 1

LOCAL_REFERENCE ?=
LINE_DB 	?=
LTR_DB		?=

# extern ../phase_2/reference.unclassified.fasta as LOCAL_REFERENCE
extern ../phase_2/reference.solo.unclassified.fasta as LOCAL_REFERENCE
extern ../phase_2/te.repbase.map as TE_REPBASE_MAP

# link to reference
reference.fasta:
	ln -sf $(LOCAL_REFERENCE) $@

# link to te sequences from repbase
te.repbase.gz:
	ln -sf $(TE_REPBASE) $@

# need to use pattern rule: %.norm.fasta: %.map %.fasta
te.fasta: te.repbase.gz
	zcat $< >$@

# from map file generated during phase_2
te.repbase.map:
	ln -sf $(TE_REPBASE_MAP)


# extract LINE only, annotated in phase_1
LINE.map: te.repbase.map
	bawk '!/^[$$,\#+]/ \
	{ if ($$2 ~ /RIL/) print $$0; }' $< >$@

# extract LTR only, annotated in phase_1
LTR.map: te.repbase.map
	bawk '!/^[$$,\#+]/ \
	{ if ($$1 ~ /\_LTR/) print $$0; }' $< >$@


# mapping of the IDs of the sequences to numbers
reference.map: reference.fasta
	cat $< \
	| fasta2tab \
	| bawk 'BEGIN {i=1;} !/^[$$,\#+]/ { print $$1,i++ }' \
	| sort -k 2,2n >$@

# rename fasta headers according to new name defined in .map file
reference.norm.fasta: reference.map reference.fasta
	cat $^2 \
	| sed -e 's/\t/ /g' -e 's/\\/ /g' -e 's/,/ /g' -e 's/\.\./ /g' \   * remove annoying characters *
	| fasta2tab \
	| translate -k $< 1 \
	| bsort \
	| tab2fasta 2 \
	| seqret -filter -supper >$@

%.norm.fasta: %.map te.fasta
	cat $^2 \
	| sed -e 's/\t/ /g' -e 's/\\/ /g' -e 's/,/ /g' -e 's/\.\./ /g' \   * remove annoying characters *
	| fasta2tab \
	| translate -k $< 1 \
	| bsort \
	| tab2fasta 2 \
	| seqret -filter -supper >$@



# new function to run RepeatMasker
define REPMASK
RepeatMasker \
-s \   * remember to change to -s *
-no_is \
-nolow \
-pa $$THREADNUM \
-lib $1 \
$2 \
&& mv $2.out $3
endef

# run RepeatMasker for LINE
.PRECIOUS: reference.LINE.fasta.out reference.LTR.fasta.out
reference.LINE.fasta.out: LINE.norm.fasta reference.norm.fasta
	!threads
	$(call REPMASK,$<,$^2,$@)

# run RepeatMasker for LTR. Depend on previous to avoid RepeatMasker to write on the 
# same reference.norm.fasta.out file
reference.LTR.fasta.out: LTR.norm.fasta reference.norm.fasta reference.LINE.fasta.out
	!threads
	$(call REPMASK,$<,$^2,$@)

# reformat RepeatMasker to Bed format
reference.%.out.bed: reference.%.fasta.out reference.norm.fasta
	tr -s ' ' \\t <$< \
	| sed 's/^[ \t]*//;s/[ \t]*$$//' \
	| bawk -v reference_fasta=$^2 'BEGIN {strand=""; start=""; end="";} /^[0-9]+/ \
	{ if ( $$9 == "C" ) { strand="-"; } \
	else if ( $$9 == "+" ) { strand="+"; } \
	print $$5,$$6,$$7,$$10,$$1,strand,$$12,$$13,"255";}' >$@


# run bedtools merge to collapse hits that are 80 bp apart
reference.%.out.bed.collapsed: reference.%.out.bed
	bedtools merge -d 80 -nms -scores median -i $< >$@


# assign deletions to the te class, on the basis of the homogeneity on match 
# found with transposable elements
reference.%.out.bed.merged: reference.%.out.bed.collapsed
	bawk -v not_merged=$(basename $@).notmerged '/^[0-9]+/ \
	{ split( $$4, TE, ";"); \   * split te names *
	consistent="TRUE"; \
	teClass=substr(TE[1], 1, 3); \   * extract the firs 3 letters (the class) *
	for (i=2; i<=length(TE); i++) { \
	if ( substr(TE[i], 1, 3) != teClass ) { consistent="FALSE"; } \	  * check consistentency *
	} \
	if ( consistent=="TRUE" ) { print $$1,$$2,$$3,teClass,$$5; } \
	else { print $$0 >not_merged; } \
	}' $< >$@


reference.%.out.bed.notmerged: reference.%.out.bed.merged
	touch $@


# check for aln length
reference.%.classified: reference.norm.fasta reference.%.out.bed.merged
	translate -a <(fasta_length <$<) 1 <$^2 \   * lenghts are put in the second column *
	| bawk -v short_aln=$(basename $@).incomplete 'function abs(x){return ((x < 0.0) ? -x : x)} \
	/^[0-9]+/ \
	{ if ( abs($$4 - $$3) >= ($$2*0.80) ) { print $$0; } \
	else { print $$0 >short_aln; } }' >$@



reference.%.incomplete: reference.%.classified
	touch $@


# Back in the original names
reference.%.classified.unmap: reference.%.classified reference.map
	cat $< \
	| translate -key_field 2 $^2 1 >$@





# extract unclassified
reference.unclassified.lst: reference.map reference.LINE.classified.unmap reference.LTR.classified.unmap
	comm -23 \
	<(cut -f1 $< | sort) \
	<(cat $^2 $^3 | cut -f1 | sort -u) >$@   * check for duplicated items, print the first one *


# extract fasta sequence of unclassified
reference.unclassified.fasta: reference.unclassified.lst reference.fasta
	fasta_get -f $< <$^2 >$@


.PHONY: test
test:
	@echo

# prerequisites of target all.
# automatically deleted when clean.full
ALL +=  reference.fasta \
	LTR.map \
	LINE.map \
	reference.map \
	LTR.norm.fasta \
	LINE.norm.fasta \
	reference.LINE.fasta.out \
	reference.LTR.fasta.out \
	reference.LINE.out.bed \
	reference.LTR.out.bed \
	reference.LINE.out.bed.collapsed \
	reference.LTR.out.bed.collapsed \
	reference.LINE.out.bed.merged \
	reference.LTR.out.bed.merged \
	reference.LINE.out.bed.notmerged \
	reference.LTR.out.bed.notmerged \
	reference.LINE.classified \
	reference.LTR.classified \
	reference.LINE.incomplete \
	reference.LTR.incomplete \
	reference.LINE.classified.unmap \
	reference.LTR.classified.unmap \
	reference.unclassified.lst \
	reference.unclassified.fasta



# clean from intermediate and temporary files
CLEAN += reference.norm.fasta \
	 LTR.norm.fasta \
	 LINE.norm.fasta \
	 reference.LINE.out.bed \
	 reference.LTR.out.bed \
	 reference.LINE.out.bed.collapsed \
	 reference.LTR.out.bed.collapsed \
	 reference.LINE.out.bed.merged \
	 reference.LTR.out.bed.merged \
	 reference.LINE.out.bed.notmerged \
	 reference.LTR.out.bed.notmerged \
	 reference.LINE.classified \
	 reference.LTR.classified \
	 reference.LINE.incomplete \
	 reference.LTR.incomplete \
	 reference.unclassified.lst \
	 $(wildcard reference.*.fasta.*)


# deletes as soon as no longer needed
# added to the CLEAN list
INTERMEDIATE += te.fasta