# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:
#	make automatic inference of tRNA.fasta with tRNAscan in master makefile
#	set distmapt correctly


MAKE := bmake
TRNA ?=
END_DIST ?= 100

extern ../phase_3/reference.unclassified.fasta as LOCAL_REFERENCE
extern ../phase_1/reference.freq as FREQ

# link to reference in gz
reference.fasta:
	ln -sf $(LOCAL_REFERENCE) $@

reference.freq:
	ln -sf $(FREQ) $@


# mapping of the IDs of the sequences to numbers
name_maps: reference.fasta
	cat $< \
	| fasta2tab \
	| bawk 'BEGIN {i=1;} !/^[$$,\#+]/ { print i++,$$1 }' \
	| sort -k 1,1n >$@

# change id of sequences to numbers
reference.norm.fasta: reference.fasta
	cat $< \
	| fasta2tab \
	| bawk 'BEGIN {i=1;} !/^[$$,\#+]/ { print i++,$$2 }' \
	| bsort -k 1,1n \
	| tab2fasta -s 2 >$@


# download tRNA sequences in fasta format
tRNA.fasta:
	wget -q -O $@ $(TRNA)



# create directory for ps_scan.
# LTR_FINDER can predict protein domains by calling ps_scan, which can be obtained from ExPASy-PROSITE
# (http://www.expasy.org/prosite/). User should place data file `prosite.dat' and ps_scan in this directory.
# If this parameter is enabled, LTR_FINDER will call them and report these protein domains if they are detected. 
ps_scan:
	mkdir -p $@; \
	cd $@; \
	wget -q -O prosite.dat ftp://ftp.expasy.org/databases/prosite/prosite.dat; \
	ln -sf $$PRJ_ROOT/local/bin/ps_scan ps_scan.pl; \
	cd ..

# for each file in reference, generate working directory, prepare it, execute makefile,
# save dst.sort, remove it
all.dst: makefile reference.norm.fasta tRNA.fasta ps_scan
	RULES_PATH="$$PRJ_ROOT/local/share/rules/phase_4.1.mk"; \
	if [ -s $$RULES_PATH ] && [ -s $< ]; then \
	  >$@; \
	  >all.ltr_finder; \
	  >all.tab; \
	  >all.ltr1; \
	  >all.ltr2; \
	  >all.aln; \
	  >all.mtx; \
	  FASTA_LST=$$(fasta2tab <$^2 | tr ' ' \\t | cut -f1 | tr [:upper:] [:lower:]); \
	  seqretsplit -filter -supper1 -sformat fasta <$^2; \
	  for SEQ in $$FASTA_LST; do \
	    DIR_NAME=$${SEQ%.*}; \
	    mkdir -p $$DIR_NAME; \
	    cd $$DIR_NAME; \
	    ln -sf ../$$SEQ.fasta reference.fasta; \
	    ln -sf ../$<; \
	    ln -sf $$RULES_PATH rules.mk; \
	    ln -sf ../$^3; \
	    ln -sf ../$^4; \
	    $(MAKE) && \
	    cat dst.sort >>../$@; \
	    cat *.ltr_finder >>../all.ltr_finder; \
	    cat *.tab >>../all.tab; \
	    cat *.ltr1 >>../all.ltr1; \
	    cat *.ltr2 >>../all.ltr2; \
	    cat aln.all >>../all.aln; \
	    cat mtx.all >>../all.mtx; \
	    cd ..; \
	    rm -r $$DIR_NAME $$DIR_NAME.fasta; \
	  done; \
	else \
	  printf "[ ERROR ] $$RULES_PATH not found!\n" >&2; \
	  exit 1; \
	fi



all.ltr_finder: all.dst
	touch $@


all.tab: all.dst
	touch $@


all.ltr1: all.dst
	touch $@


all.ltr2: all.dst
	touch $@

all.aln: all.dst
	touch $@


all.mtx: all.dst
	touch $@

#include dir_lst.mk



# # execute bmake on each subdirectory
# .PHONY: subsystem
# subsystem: dir_lst.mk
# 	@echo Looking into subdirs: $(MAKE)
# 	for D in $(DIR_LST); do \
# 	  if [ -d $$D ]; then \
# 	    cd $$D; \
# 	    $(MAKE); \
# 	    cd ..; \
# 	  fi; \
# 	done


# get all distance from subdirs
# all.dst: subsystem
# 	>$@; \
# 	for D in $(DIR_LST); do \
# 	  if [ -s $$D/dst.sort ]; then \
# 	    cd $$D; \
# 	    cat dst.sort >>../$@; \
# 	    cd ..; \
# 	  fi; \
# 	done; \

# filter and sort distances by coordinates
# report only ltrs that are END_DIST bp away from the ends
all.dst.sort: all.dst
	UNCLASSIFIED=$(basename $@).far; \
	>$$UNCLASSIFIED; \   * reset file *
	bawk -v far=$$UNCLASSIFIED -v end_dist=$(END_DIST) 'function abs(x){return ((x < 0.0) ? -x : x)} !/^[$$,\#+]/ \
	{ if ( abs($$4-$$2) <= end_dist  && abs($$3-$$5) <= end_dist ) { print $$0; } \
	else { print $$0 >>far } }' $< \
	| set_collapse -o 6 \
	| bsort --key=1,1n --key=2,2n --key=4,4n --key=5,5n >$@


# ltrs with ends that are far from extremities
all.dst.far: all.dst.sort
	touch $@


# translate chr names from number to the original names
all.dst.sort.map: name_maps all.dst.sort
	translate $< 1 <$^2 >$@


# extract list of remaining sequences
reference.not_found.freq: name_maps all.dst.sort.map reference.freq
	comm -23 <(cut -f2 $< | sort) <(cut -f1 $^2 | sort) \
	| translate -a $^3 1 >$@


reference.not_found.freq.plot.pdf: reference.not_found.freq
	distrib_plot -t histogram -o $@ 3 <$<


# extract fasta of remaining sequences
reference.not_found.fasta: reference.not_found.freq reference.fasta
	fasta_get -f <(cut -f 1 $<) <$^2 >$@

.PHONY: test
test:
	
	echo $$FASTA_LST; \

# prerequisites of target all.
# automatically deleted when clean.full
ALL +=  reference.fasta \
	tRNA.fasta \
	name_maps \
	all.dst \
	all.dst.sort \
	all.dst.sort.map \
	all.ltr_finder \
	all.dst.far \
	reference.not_found.freq \
	reference.not_found.fasta \
	all.tab \
	all.ltr1 \
	all.ltr2 \
	all.aln \
	all.mtx 


# clean from intermediate and temporary files
CLEAN += $(wildcard *.fasta) \
	 ps_scan

# add dipendence to clean targhet
clean: clean_dir

.PHONY: clean_dir
clean_dir:
	@echo cleaning up...
	for D in $(DIR_LST); do \
	  if [ -d $$D ]; then \
	    cd $$D; \
	    $(MAKE) clean; \
	    cd ..; \
	  fi; \
	done


# deletes as soon as no longer needed
# added to the CLEAN list
INTERMEDIATE += reference.norm.fasta

