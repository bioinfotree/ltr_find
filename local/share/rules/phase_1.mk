# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:
#	creare di qua il file di mappaggio delle sequenze reference; tale file verrà poi usato in tutti gli stadi successivi
#	creare di qua il file di mappaggio dei nomi degli elementi trasponibili alla nuova nomenclatura

# parameters for tandem repeat finder
TRF_PARAM ?= 2 7 7 80 10 50 500 



# link to reference
reference.fasta.gz:
	ln -sf $(DELETIONS_FASTA) $@

# list of references; needed for extract frequences
reference.fasta.lst:
	ln -sf $(DELETIONS_LST) $@

# trasform reference to fasta
reference.fasta: reference.fasta.gz
	zcat $< >$@

# get frequences
reference.freq: reference.fasta.lst
	awk -F ' ' '!/^[$$,\#+]/ \
	{ split("", cnt); \   * reset the array *
	split($$6,a,"-"); \
	for (i in a) { cnt[a[i]]=a[i]; } \
	printf "%s:%i-%i\t",$$1,$$2,$$3; \
	for (i in cnt) { printf "%s;",cnt[i] }; \
	printf "\t%i\n",length(cnt); }' $< >$@


# # test performances of fasta splitter
# 
# # 2.49user 0.25system 0:03.89elapsed 70%CPU
# chunks: reference.fasta
# 	/usr/bin/time splitMultifasta $< 1000
# 
# #1.62user 0.39system 0:04.41elapsed 45%CPU
# chunks2: reference.fasta
# 	/usr/bin/time fasta2tab <$< \
# 	| split -l 1000 -d - $<. 
# 
# # 0.07user 0.27system 0:02.15elapsed 16%CPU
# chunks1: reference.fasta
# 	/usr/bin/time gt splitfasta -numfiles 8 -force yes $<

# redirects sequences that has >=80 N to new file
define CLEAN_N
	cat $1 \
	| sed '/^$$/d' \
	| fasta2tab \
	| bawk '!/^[$$,\#+]/ \
	{ seqlen=length($$2); \
	line_tmp=$$0; \
	gsub("N","",$$2); \
	seqlensub=length($$2); \
	if ( seqlensub/seqlen > 0.2 ) {print line_tmp;} \
	else { print $$1 >"$(basename $@).$2" } }' \
	| bsort \
	| tab2fasta 2
endef

# split to multiple fasta in order to parallelize
#FASTA_CHUNCKS := $(addprefix reference.fasta.,$(shell seq 8))

chunks.mk:
	printf "FASTA_CHUNCKS := $(addprefix reference.fasta.,$(shell seq 8))" >$@

include chunks.mk


reference.fasta.1: reference.fasta
	gt splitfasta -numfiles 8 -force yes $<

# redirects sequences that has >=80 N to new file
NO_UNDEF_CHUNCKS = $(addsuffix .noundef,$(FASTA_CHUNCKS))
reference.fasta.%.noundef: reference.fasta.1
	$(call CLEAN_N,$(basename $<).$*,N) \
	>$@



N_CHUNKS = $(NO_UNDEF_CHUNCKS:.noundef=.N)
reference.fasta.%.N: reference.fasta.%.noundef
	touch $@

reference.fasta.N.all: $(N_CHUNKS)
	cat $^ >$@

# search for repeats
# produces file of masked sequences
TRF_CHUNCKS = $(addsuffix .trf,$(NO_UNDEF_CHUNCKS))
reference.fasta.%.noundef.trf: reference.fasta.%.noundef
	trf407b $< $(TRF_PARAM) -h -ngs -m >$@


MASKED_CHUNCKS = $(TRF_CHUNCKS:.trf=.mask)
# trick to substitute space in makefile
NOTHING:=
SPACE:=$(NOTHING) $(NOTHING)
# rename reference.fasta.8.noundef.2.7.7.80.10.50.500.mask 
# to reference.fasta.8.noundef.mask
reference.fasta.%.mask: reference.fasta.%.noundef.trf
	cp $(basename $<).$(subst $(SPACE),.,$(TRF_PARAM)).mask $@; \
	touch $@; \
#	rm $(basename $<).$(subst $(SPACE),.,$(TRF_PARAM)).mask


# redirects sequences that has >=80 N (due to repeats) to new file
reference.fasta.cleaned.gz: $(MASKED_CHUNCKS)
	$(call CLEAN_N,$^,repeat) \
	| gzip -c >$@

# contains sequences that has >=80 N due to repeats
reference.fasta.cleaned.repeat: reference.fasta.cleaned.gz
	touch $@



.PHONY: test
test:
	@echo $(FASTA_CHUNCKS )

# prerequisites of target all.
# automatically deleted when clean.full
ALL +=  reference.fasta.gz \
	reference.fasta.lst \
	reference.freq \
	$(NO_UNDEF_CHUNCKS) \
	$(N_CHUNKS) \
	reference.fasta.N.all \
	$(TRF_CHUNCKS) \
	$(MASKED_CHUNCKS) \
	reference.fasta.cleaned.gz \
	reference.fasta.cleaned.repeat
	


# clean from intermediate and temporary files
CLEAN += $(FASTA_CHUNCKS) \
	 $(wildcard *.mask) \
	 chunks.mk

# deletes as soon as no longer needed
# added to the CLEAN list
INTERMEDIATE += $(N_CHUNKS) \
		reference.fasta