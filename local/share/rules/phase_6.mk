# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:



EVAL_BLAST ?= 1e-06
BLAST_DB ?= /iga/blast-db/db/ncbi/nr


#extern ../phase_4/reference.not_found.fasta as LOCAL_REFERENCE

# link to reference in gz
reference.fasta:
	zcat $(DELETIONS_FASTA) >$@

te.repbase.gz:
	ln -sf $(TE_REPBASE) $@

te.others.gz:
	ln -sf $(TE_OTHERS) $@


# map between name of transposable elements in RepBase end name defined by Wicker et al.
te.repbase.map: te.repbase.gz
	zcat $< \
	| sed -e 's/\t/ /g' -e 's/\\/ /g' -e 's/,/ /g' -e 's/\.\./ /g' \   * remove annoying characters *
	| fasta2tab \
	| bawk 'function randint(n) { return int(n * rand()); } \
	BEGIN {i=1; IGNORECASE=1;} !/^[$$,\#+]/ \
	{  i++; \
	if ( $$1 ~ /Gypsy/ || $$1 ~ /GYP/ ) { lab="RLG"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Cop/ ) { lab="RLC"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Bel-Pao/ ) { lab="RLB"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Retrovirus/ ) { lab="RLR"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /ERV/ ) { lab="RLE"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /DIRS/ ) { lab="RYD"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Ngaro/ ) { lab="RYN"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /VIPER/ ) { lab="RYV"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /RTE/ ) { lab="RIT"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Jocket/ ) { lab="RIJ"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ / L1 / || $$1 ~ /LINE/ || $$1 ~ /RIL/ ) { lab="RIL"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /SINE/ ) { lab="RSX"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /tRNA/ ) { lab="RSX"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /7SL/ ) { lab="RSX"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /5S/ ) { lab="RSX"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Mariner/ ) { lab="DTT"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /hAT / ) { lab="DTA"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Mutator/ || $$1 ~ /MuDR/ ) { lab="DTM"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Merlin/ ) { lab="DTE"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Transib/ ) { lab="DTR"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /PiggyBac/ ) { lab="DTB"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Harb/ || $$1 ~ /PIF/ ) { lab="DTH"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /CACTA/ || $$1 ~ /EnSpm/ ) { lab="DTC"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Crypton/ ) { lab="DYC"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Helitron/ ) { lab="DHH"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Maverick/ ) { lab="DMM"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else if ( $$1 ~ /Penelope/ ) { lab="RPP"; printf "%s\t%s_%s\n",$$1,lab,i; } \
	else {print $$1 >"te.repbase.unamed";} }' >$@   * seve to file sequences that can't be classified *


# contains sequences from repbase tha can't be 
# classified with the name defined by Wicker et al
te.repbase.unamed: te.repbase.map
	touch $@


# rename headers of te from RepBase according to new name defined by Wicker et al.
te.repbase.fasta: te.repbase.map te.repbase.gz
	zcat $^2 \
	| sed -e 's/\t/ /g' -e 's/\\/ /g' -e 's/,/ /g' -e 's/\.\./ /g' \   * remove annoying characters *
	| fasta2tab \
	| translate -k $< 1 \
	| bsort \
	| tab2fasta 2 \
	| seqret -filter -supper >$@

# unzip and remove annoying charachters
te.others.fasta: te.others.gz
	zcat $< \
	| fasta2tab \
	| bawk '!/^[$$,\#+]/ \
	{ gsub(/[^[:alnum:],\_]/," ",$$1); \
	split($$1,a," "); \
	print a[1],toupper($$2); }' \   * remove annoying characters *
	| tab2fasta -s 2 >$@


# mapping of the IDs of the sequences to numbers
reference.map: reference.fasta
	cat $< \
	| fasta2tab \
	| bawk 'BEGIN {i=1;} !/^[$$,\#+]/ { print $$1,i++ }' \
	| sort -k 2,2n >$@

# rename fasta headers according to new name defined in .map file
reference.norm.fasta: reference.map reference.fasta
	cat $^2 \
	| sed -e 's/\t/ /g' -e 's/\\/ /g' -e 's/,/ /g' -e 's/\.\./ /g' \   * remove annoying characters *
	| fasta2tab \
	| translate -k $< 1 \
	| bsort \
	| tab2fasta 2 \
	| seqret -filter -supper >$@


RLC.fasta: te.others.fasta.gz
	zcat $< \
	| fasta_get -r "RLC" >$@



meme_test: RLC.fasta
	/usr/bin/time -p \
	meme $< \
	-dna \   * sequences use DNA alphabet *
	-oc $@ \   * name of directory for output files will replace existing directory *
	-mod anr \   * distribution of motifs *
	-maxsize 5300000 \   * maximum dataset size in characters *
	-nmotifs 5 \   * number of *different* motifs to search for *
	-minw 8 \   * min width of the motif
	-maxw 200 \   * max width of the motif
	-evt 0.0001 \   * stop if motif E-value greater than <evt> *
	-minsites 2 \   * minimum number of sites for each motif *
	-maxsites 10 \   * maximum number of sites for each motif *
	-revcomp \   *  motifs occurrences can be on the given DNA strand or on its reverse complement *
	-p 24   * use <p> processors *


glam2_test: te.fasta
	/usr/bin/time \
	glam2 \   * alphabets *
	-O $@ \   * output directory allow clobbering *
	-2 \   * search both strands *
	-p \   * print progress information at each iteration *
	-x 0 \   * site sampling algorithm: 0=FAST 1=SLOW 2=FFT (0) *
	n $<

proteins.xml.gz: reference.fasta
	blastx \
	-evalue $(EVAL_BLAST) \
	-outfmt 5 \
	-max_target_seqs 20 \
	-query $< \
	-remote \
	-db nr \
	-out $(basename $@); \
	if gzip -cv $(basename $@) >$@; then \
	rm $(basename $@); \
	fi

test_refTEs.fa: te.repbase.fasta te.others.fasta
	cat $< $^2 >$@

test.fa: reference.norm.fasta
	cp $< $@

export REPET_PATH := /home/mvidotto/bioinfotree/binary/appliedgenomics/prj/ltr_find/local/stow/REPET_linux-x64-2.2
annot_S1: test_refTEs.fa test.fa
	TEannot.py \
	-P test \
	-C TEannot.cfg \
	-S 1

annot_S2:
	TEannot.py \
	-P test \
	-C TEannot.cfg \
	-S 2 \
	-a BLR


# 	TEannot.py \
# 	-P test \
# 	-C TEannot.cfg \
# 	-S 2 \
# 	-a CM
# 	TEannot.py \
# 	-P test \
# 	-C TEannot.cfg \
# 	-S 2 \
# 	-a CM


.PHONY: test
test:

# prerequisites of target all.
# automatically deleted when clean.full
ALL += 

# clean from intermediate and temporary files
CLEAN += $(wildcard meme_test*)

# deletes as soon as no longer needed
# added to the CLEAN list
INTERMEDIATE += 
