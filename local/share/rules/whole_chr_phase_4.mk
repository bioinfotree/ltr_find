# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:
#	make automatic inference of tRNA.fasta with tRNAscan in master makefile
#	subsystem cleaning
#	reference name and tRNA name passed as vaariable name


MAKE := bmake
REFERENCE ?=
TRNA ?=



# link to reference in gz
reference.gz:
	ln -sf $(REFERENCE) $@


# mapping of the IDs of the sequences to numbers
name_maps: reference.gz
	zcat $< \
	| fasta2tab \
	| bawk 'BEGIN {i=1;} !/^[$$,\#+]/ { print i++,$$1 }' \
	| sort -k 2,2n >$@


# change id of sequences to numbers
reference.norm.fasta: reference.gz
	zcat $< \
	| fasta2tab \
	| bawk 'BEGIN {i=1;} !/^[$$,\#+]/ { print i++,$$2 }' \
	| bsort -k 1,1n \
	| tab2fasta -s 2 >$@


# download tRNA sequences in fasta format
tRNA.fasta:
	wget -q -O $@ $(TRNA)



# create directory for ps_scan.
# LTR_FINDER can predict protein domains by calling ps_scan, which can be obtained from ExPASy-PROSITE
# (http://www.expasy.org/prosite/). User should place data file `prosite.dat' and ps_scan in this directory.
# If this parameter is enabled, LTR_FINDER will call them and report these protein domains if they are detected. 
ps_scan:
	mkdir -p $@; \
	cd $@; \
	wget -q -O prosite.dat ftp://ftp.expasy.org/databases/prosite/prosite.dat; \
	ln -sf $$PRJ_ROOT/local/bin/ps_scan ps_scan.pl; \
	cd ..

# Prepare directories and generate directories list
dir_lst.mk: makefile reference.norm.fasta tRNA.fasta ps_scan
	RULES_PATH="$$PRJ_ROOT/local/share/rules/phase_4.1.mk"; \
	if [ -s $$RULES_PATH ] && [ -s $< ]; then \
	  printf "DIR_LST :=" >$@; \
	  FASTA_LST=$$(fasta2tab <$^2 | tr ' ' \\t | cut -f1 | tr [:upper:] [:lower:]); \
	  cat $^2 | seqretsplit -filter -supper1 -sformat fasta; \
	  for SEQ in $$FASTA_LST; do \
	    FILENAME=$${SEQ%.*}; \
	    mkdir -p $$FILENAME; \
	    cd $$FILENAME; \
	    ln -sf ../$$SEQ.fasta reference.fasta; \
	    ln -sf ../$<; \
	    ln -sf $$RULES_PATH rules.mk; \
	    ln -sf ../$^3; \
	    ln -sf ../$^4; \
	    cd ..; \
	    printf " $$FILENAME" >>$@; \
	  done; \
	fi



include dir_lst.mk



# execute bmake on each subdirectory
.PHONY: subsystem
subsystem: dir_lst.mk
	@echo Looking into subdirs: $(MAKE)
	for D in $(DIR_LST); do \
	  if [ -d $$D ]; then \
	    cd $$D; \
	    $(MAKE); \
	    cd ..; \
	  fi; \
	done


# get all distance from subdirs
all.dst: subsystem
	>$@; \
	for D in $(DIR_LST); do \
	  if [ -s $$D/dst.sort ]; then \
	    cd $$D; \
	    cat dst.sort >>../$@; \
	    cd ..; \
	  fi; \
	done; \

# filter and sort distances by coordinates
all.dst.sort: all.dst
	bawk '!/^[$$,\#+]/ { print $$0 }' $< \
	| set_collapse -o 6 \
	| bsort --key=1,1n --key=2,2n --key=4,4n --key=5,5n >$@

# translate chr names from number to the original names
all.dst.sort.map: name_maps all.dst.sort
	translate $< 1 <$^2 >$@




.PHONY: test
test:
	
	echo $$FASTA_LST; \

# prerequisites of target all.
# automatically deleted when clean.full
ALL +=  reference.gz \
	tRNA.fasta \
	name_maps \
	dir_lst.mk \
	$(DIR_LST) \
	subsystem \
	all.dst.sort \
	all.dst.sort.map




# clean from intermediate and temporary files
CLEAN += $(wildcard *.fasta)

# add dipendence to clean targhet
clean: clean_dir

.PHONY: clean_dir
clean_dir:
	@echo cleaning up...
	for D in $(DIR_LST); do \
	  if [ -d $$D ]; then \
	    cd $$D; \
	    $(MAKE) clean; \
	    cd ..; \
	  fi; \
	done


# deletes as soon as no longer needed
# added to the CLEAN list
INTERMEDIATE += all.dst \
		reference.norm.fasta

