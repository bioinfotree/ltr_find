# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:
# rerun repeatmasker in order to classifie other LINE or single LTR that have been missed..

THIS_REFERENCE ?= ../phase_2/reference.unclassified.fasta



# link to reference
reference.fasta:
	ln -sf $(THIS_REFERENCE) $@

random_lines:
	sort -R $(THIS_REFERENCE) | sort -R | sort -R >$@

first%_lines: random_lines
	head -n $* $< >$@


last%_lines: random_lines
	tail -n $* $< >$@

# create the fasta file containing the remaining deletions
5_phase_unclassified.fasta: 5_phase_unclassified_lst reference.fasta
	fasta_get -f $< <$^2 >$@



.PHONY: test
test:
	@echo

# prerequisites of target all.
# automatically deleted when clean.full
ALL +=	


# clean from intermediate and temporary files
CLEAN += 

# deletes as soon as no longer needed
# added to the CLEAN list
INTERMEDIATE += 